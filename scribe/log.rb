class ::Scribe
	def plog text
		text = Time.now.strftime '%Y-%m-%d %H:%M:%S' + ": " + text
		@@log << text
		@@log_file = default_file('plog') if @@log_file.nil?
		@@log_file.puts text if @@log_file.respond_to? :puts
		return
	end
	def clog text
		return unless @@clog_logging
		@@cmds << text
		@@cmds_file = default_file('clog') if @@cmds_file.nil?
		@@cmds_file.puts text if @@cmds_file.respond_to? :puts
		return
	end
	def default_file ext
		begin
			ext = ext.to_s
			return nil if ext.empty?
			Dir.mkdir "#{ext}s" unless Dir.entries('.').include? "#{ext}s"
			return File.open("#{ext}s/#{Time.now.strftime '%Y-%m-%d %H:%M:%S'}.#{ext}",'w')
		rescue
			return nil
		end
	end
	def combat_log
		background gradient(bisque,white)
		stack do
			button "Copy to Clipboard", :width=>1.0 do
				clipboard = @@log.join "\n"
			end
			button "Save to File", :width=>1.0 do
				fname = ask_save_file
				return if fname.nil?
				begin
					File.open(fname, "w") {|f| f << @@log.join("\n") }
				rescue
					alert("Error: Unable to save!")
				end
			end
			@@log.each do |line|
				inscription line, :margin=>[0,0,0,0]
			end
		end
	end
	def history
		background gradient(bisque,white)
		stack do
			@@cmds.each do |line|
				inscription line.strip, :margin=>[0,0,0,0] unless line.strip.empty?
			end
		end
	end
end
