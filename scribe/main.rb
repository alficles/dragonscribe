class ::Scribe
	@@chars = []
	@@player_select = ''
	@@global_edit = nil
	@@log = []
	@@cmds = []
	@@clog_logging = true
	@@cmds_file = nil
	@@log_file = nil
	@@update_source = nil
	@@loaded_file = false

	url '/', :index
	url '/help', :help
	url '/help/global', :help_global
	url '/help/player', :help_player
	url '/log', :combat_log
	url '/cmds', :history

	def index
		unless @@loaded_file then
			@@loaded_file = true
			unless ARGV[1].nil? then
				open ARGV[1]
			end
		end
		background gradient(bisque,white)
		@@player_stack = stack { players }
		keypress {|key| main_keypress(key) }
	end

	def main_keypress(key)
			if (key == :escape)
				@@chars.each{|p| p[:selected] = false}
				@@global_edit = nil
				refresh_players
				return
			end
			return unless @@global_edit.nil?
			active_edit = @@chars.detect{|char| not char[:edit].nil?}
			return unless active_edit.nil?

			if (key == :control_n) then
				p = new_char
				clog 'create.'
				refresh_players
				player_click p[:name]
				return
			end

			if (key == :control_s) then
				save
				return
			end

			if (key == :control_o) then
				open
				refresh_players
				return
			end

			if (key == :control_l) then
				Shoes.app '/log', :width=>400, :height=>800
				return
			end
			if (key == :control_h) then
				Shoes.app '/cmds', :width=>400, :height=>800
				return
			end
	
			if (key == :control_a) then
				@@chars.each{|p| p[:selected] = true}
				refresh_players
			end
			
			if (key == :down) then
				advance
				clog 'next'
				refresh_players
			end

			if (key == :up) then
				reverse
				clog 'prev'
				refresh_players
			end

			if (key == '!')
				open 'test1.init'
				refresh_players
				return
			end

			if (key == :f1)
				open_help
				return
			end

			if (key == '`') then
				open_global_edit
				return
			end

			if key.kind_of? Symbol then
				@@player_select = ''
				refresh_players
			else
				@@player_select += key

				p = @@chars.select{|char| char[:name] =~ /^#{@@player_select}/i}
				if p.empty? then
					@@player_select = ''
					refresh_players
				elsif p.length == 1
					@@player_select = ''
					refresh_players
					player_click p[0][:name]
				else
					refresh_players
				end
			end
	end

	def open_global_edit
		return unless @@global_edit.nil?
		@@player_stack.prepend do
			flow do
				@@global_edit = edit_line "", :width=>"100%"
				keypress do |key|
					if (key == "\n" or key == :enter)
						do_global_edit @@global_edit.text
						@@global_edit = nil
						refresh_players
						open_global_edit if @@chars.detect{|char| char[:selected]}
					end
				end
			end
		end
	end
	


	def new_char
			@@chars << {
				:name=>'',
				:init=>0,
				:hp=>0,
				:maxhp=>0,
				:temphp=>0,
				:is_mob=>true,
				:conditions=>[],
				:selected=>false,
				:active=>false
			}
			@@chars[-1]
	end

	def marshal
		"clear\n"+@@chars.map{|p| "create name:#{p[:name].to_s}. init:#{p[:init]}. hp:#{p[:hp]}. maxhp:#{p[:maxhp]}. temphp:#{p[:temphp]}. #{p[:conditions].map{|c|"cond:#{c}."}.join ' '} #{p[:is_mob]?"player:false":"player:true"}\n"}.join
	end

	def save
		fname = ask_save_file
		return if fname.nil?
		begin
			File.open(fname, "w") {|f| f << marshal }
			plog "Saved to '#{fname}'."
		rescue
			alert("Error: Unable to save!")
		end
	end

	def open(fname = nil)
		fname = ask_open_file if fname.nil?
		return if fname.nil?
		begin
			File.open(fname, "r") do |f|
				f.each do |cmd|
					do_global_edit cmd
				end
			end
		rescue
			alert("Error: Unable to open!")
		end
	end
	public :open
end
