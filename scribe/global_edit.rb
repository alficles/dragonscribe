class ::Scribe
	def do_global_edit(cmd)
		case (cmd.strip)
		when 'roll'
			roll_init
			plog "Rolled initiative."
			@@chars.each {|char| clog "edit #{char[:name].to_s}.init:#{char[:init].to_s}"}
			clog 'sort'
		when 'sort'
			sort_init
			plog "Sorted by initiative."
			clog 'sort'
		when 'clear'
			@@chars.clear
			plog "Cleared."
			clog 'clear'
		when 'save'
			save
			plog "Saved."
			clog 'save'
		when 'load'
			open
			plog "Loaded."
			clog 'load'
		when 'next'
			advance
			clog 'next'
		when 'prev'
			reverse
			clog 'prev'
		when /^create *(.*)$/
			plog "Created new."
			old_logging = @@clog_logging
			@@clog_logging = false
			do_player_edit new_char, $1
			@@clog_logging = old_logging
			clog cmd
		when /^edit *([^.]*)\.(.*)$/
			p = @@chars.detect{|char| char[:name].downcase == $1.downcase}
			do_player_edit p, $2 unless p.nil?
		when 'help'
			open_help
		else
			@@chars.select{|c| c[:selected]}.each{|p| do_player_edit(p, cmd) }
		end
	end


	def advance
		init_move(1)
	end
	def reverse
		init_move(-1)
	end
	def init_move(step)
		active = nil
		@@chars.each_with_index{|char,idx|
			if char[:active] and active.nil? then
				active = idx
			end
		}
		old_active = active
		if active.nil? then
			@@chars[0][:active] = true if @@chars.length > 0
		else
			@@chars[active][:active] = false
			start_act = active
			active = (active + step) % @@chars.length
			curhp = @@chars[active][:maxhp] - @@chars[active][:hp]
			#debug active
			while (curhp <= 0 and
					(@@chars[active][:is_mob] or curhp <= -(@@chars[active][:maxhp]/2)) and
					start_act != active) do
				active = (active + step) % @@chars.length
				curhp = @@chars[active][:maxhp] - @@chars[active][:hp]
				#debug active
			end
			@@chars[active][:active] = true
		end
		if active != old_active then
			plog "Start of turn: #{@@chars[active][:name]}."
			end_of_turn @@chars[old_active] if step == 1
			start_of_turn @@chars[active] if step == 1
		end
	end

	def roll_init
		@@chars.each do |char|
			char[:ibonus] ||= 0
			char[:init] = char[:ibonus] + (rand(20) + 1)
		end
		sort_init
	end
	
	def sort_init
		@@chars.each {|char| char[:init] ||= 0 }
		@@chars.sort!{|a,b| b[:init] <=> a[:init]}
	end
end
