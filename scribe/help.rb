class ::Scribe
	def open_help
		Shoes.app '/help', :width=>450, :height=>600, :resizable=>true
	end
	def help
		if ($last_help == :player) then
			visit '/help/player'
		else
			visit '/help/global'
		end
	end

	def help_global
		$last_help = :global
		background gradient(bisque,white)
		stack do
			tagline "Quick Reference", :font=>"bold"
			help_nav :global
			help_title "roll"
			help_text "Roll initiative!"
			help_title "sort"
			help_text "Sort on inititive."
			help_title "clear"
			help_text "Remove everything."
			help_title "save"
			help_text "Open a save dialog."
			help_title "load"
			help_text "Open a load dialog."
			help_title "create {player_edit_commands}"
			help_text "Creates a new mob, initializing it with the given edit commands."
		 	help_title "edit {name}. {player_edit_commands}"
		 	help_text "Edit {name}, using {player_edit_commands}."
			help_title "help"
			help_text "Open this dialog."
			help_title "{player_edit_commands}"
			help_text "Apply the commands to every selected creature."
		end
	end
	def help_player
		$last_help = :player
		background gradient(bisque,white)
		stack do
			tagline "Quick Reference", :font=>"bold"
			help_nav :player
			help_text "Player commands are period-terminated. You can give as many commands as you want on a single line."
			help_title "Tilde Completion"
			help_text "A word ending in a tilde (~) will be automatically converted to the name of the player or mob whose name matches the start of the word. For example, 'tha~' will be converted to 'Thaco' if Thaco is in the party."
			help_title "Special Conditions"
			help_text "Conditions that end with 'eot Foo' or 'bot Foo' will be automatically removed at the end or beginning Foo's turn, respectively. 'eont Foo' will be converted to 'eot Foo' at the end of Foo's turn."
			help_title "{number}"
			help_text "Do damage (or healing)."
			help_title "in[it]: {number}"
			help_text "Set the initiative value."
			help_title "h[p]: {number}"
			help_text "Set current hit points."
			help_title "m[axhp]: {number}"
			help_text "Set maximum hit points."
			help_title "t[emphp]: {number}"
			help_text "Apply temporary hit points."
			help_title "i[bonus]: {number}"
			help_text "Set the initiative bonus."
			help_title "n[ame]: {text}"
			help_text "Set the name."
			help_title "c[ond]: {text}"
			help_text "Add a condition."
			help_title "c[ond]-: {text}"
			help_text "Remove conditions that include the give text."
			help_title "c[ond]-: /{regexp}/"
			help_text "Remove conditions that match the given regular expression. Only very basic regexps are supported."
			help_title "p[layer]"
			help_text "Mark the creature as a player."
			help_title "p[layer]: f[alse]"
			help_text "Mark the creature as a mob."
			help_title "d[elay]: {name}"
			help_text "Delay until after the named creature. The name may be abbreviated to the shortest unique prefix."
			help_title "d[elay]: /{regexp}/"
			help_text "Delay until after the creature that matches the regular expression. Only very basic regexps are supported."
			help_title "d[elay]: +/-{number}"
			help_text "Delay relative to the creature's starting position."
			help_title "delete"
			help_text "Delete the creature."
		end
	end
	def help_title text, *args
		para text, args, :font=>"bold", :margin=>[10,10,0,0]
	end
	def help_text text, *args
		inscription text, args, :margin=>[25,0,0,0]
	end
	def help_nav active
		flow do
			flow :width=>0.5 do
				if (active == :global) then
					background gradient(midnightblue, slateblue)
					para "Global", :align=>"center", :margin=>[0,0,0,0], :stroke=>white
				else
					background gradient(midnightblue(0.3), slateblue(0.3))
					para "Global", :align=>"center", :margin=>[0,0,0,0], :stroke=>black
				end
				border black, :strokewidth=>1
				click { visit '/help/global' }
			end
			flow :width=>0.5 do
				if (active == :global) then
					background gradient(midnightblue(0.3), slateblue(0.3))
					para "Player", :align=>"center", :margin=>[0,0,0,0], :stroke=>black
				else
					background gradient(midnightblue, slateblue)
					para "Player", :align=>"center", :margin=>[0,0,0,0], :stroke=>white
				end
				border black, :strokewidth=>1
				click { visit '/help/player' }
			end
		end
	end
end
