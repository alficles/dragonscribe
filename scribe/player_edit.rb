class ::Scribe
	def do_player_edit(p, cmd)
		cmd.split('.').map{|clause| clause.strip}.each do |clause|
			clause.gsub!(/[^ :]+~/i) do |prefix|
				c = @@chars.detect{|char| char[:name][0...prefix.length-1].downcase == prefix[0...-1].downcase}
				#debug c[:name][0...prefix.length-1].downcase
				#debug prefix[0...-1].downcase
				if c.nil? then
					prefix
				else
					c[:name]
				end
			end
			k,v = clause.split ':'
			#debug clause
			clog "edit #{p[:name].to_s}.#{clause}"
			case k.downcase.strip
			when /^[-+]?[0-9]+$/
				k = k.to_i
				plog "#{k.to_s} damage to #{p[:name]}." if k > 0
				plog "#{(-k).to_s} healing to #{p[:name]}." if k < 0
				p[:hp] = 0 if p[:hp].nil?
				p[:hp] = p[:maxhp] if (k < 0 and p[:hp] > p[:maxhp])
				if (k > 0) then
					p[:temphp] -= k
					if (p[:temphp] < 0) then
						k = -p[:temphp]
						p[:temphp] = 0
					else
						k = 0
					end
				end
				p[:hp] += k
			when /^in(?:i(?:t)?)?$/
				plog "Initiative set to #{v} for #{p[:name]}."
				p[:init] = v.to_i
			when /^h(?:p)?$/
				plog "Total damage set to #{v} for #{p[:name]}."
				p[:hp] = v.to_i
			when /^m(?:a(?:x(?:h(?:p)?)?)?)?$/
				plog "Max hit points set to #{v} for #{p[:name]}."
				p[:maxhp] = v.to_i
			when /^t(?:e(?:m(?:p(?:h(?:p)?)?)?)?)?$/
				plog "Temporary hit points set to #{v} for #{p[:name]}."
				p[:temphp] = v.to_i
			when /^i(?:b(?:o(?:n(?:u(?:s)?)?)?)?)?$/
				plog "Initiative bonus set to #{v} for #{p[:name]}."
				p[:ibonus] = v.to_i
			when /^n(?:a(?:m(?:e)?)?)?$/
				newname = v.strip
				if @@chars.detect{|char| char[:name] == newname}.nil? then
					plog "Name changed from #{p[:name]} to #{v.strip}."
					p[:name] = v.strip
				else
					debug "Name update failed: #{newname.inspect}"
				end
			when /^c(?:o(?:n(?:d)?)?)?$/
				plog "#{p[:name]} gains '#{v.strip}'"
				p[:conditions] << v.strip
			when /^c(?:o(?:n(?:d)?)?)?-$/
				if (v.strip =~ /^\/(.*)\//) then
					r = Regexp.new($1, true)
					plog "Removing conditions from #{p[:name]} matching /#{$1}/"
					p[:conditions].delete_if{|cond| cond =~ r}
				elsif (v.strip =~ /^#([0-9]+)/)
					plog "Removing condition from #{p[:name]} '#{p[:conditions][$1.to_i]}'"
					p[:conditions].delete_at $1.to_i
				else
					plog "Removing conditions from #{p[:name]} matching '#{$1}'"
					p[:conditions].delete_if{|cond| cond.include? v.strip}
				end
			when /^p(?:l(?:a(?:y(?:e(?:r)?)?)?)?)?$/
				if (v.nil? or v =~ /t(?:rue)?/ or v.empty?) then
					plog "#{p[:name]} is a PC."
					p[:is_mob] = false
				else
					plog "#{p[:name]} is an NPC."
					p[:is_mob] = true
				end
			when /^d(?:e(?:l(?:a(?:y)?)?)?)?$/
				targ = nil
				v = v.strip
				if (v =~ /^\/(.*)\//) then
					r = Regexp.new($1, true)
					targ = @@chars.detect{|c| c[:name].downcase =~ r}
				elsif (v =~ /^([+-][0-9]+)/)
					@@chars.each_with_index{|c,idx|
						targ = @@chars[(v.to_i+idx)% @@chars.length] if c[:name] == p[:name]
					}
				else
					targ = @@chars.detect {|c| c[:name][0...(v.length)].downcase == v.downcase}
				end
				unless targ.nil? or targ[:name] == p[:name]
					plog "#{p[:name]} delayed until after #{targ[:name]}."
					#debug targ[:name]
					#debug @@chars.map{|c| c[:name]}.inspect
					@@chars.delete_if{|char| char[:name] == p[:name]}
					#debug @@chars.map{|c| c[:name]}.inspect
					idx = @@chars.index targ
					@@chars.insert((idx+1)% (@@chars.size+1) , p) unless idx.nil?
					p[:conditions] << 'Completed Beginning of Turn'
					#debug @@chars.map{|c| c[:name]}.inspect
				else
					debug "Targ is nil"
				end
			when /^delete$/
				plog "#{p[:name]} deleted."
				@@chars.delete_if{|char| char[:name] == p[:name]}
			else
				plog "#{p[:name]} gains '#{k.strip}'"
				p[:conditions] << k.strip
			end
		end
	end

	def start_of_turn player
		old_logging = @@clog_logging
		@@clog_logging = false
		if player[:conditions].include? 'Completed Beginning of Turn' then
			player[:conditions].delete 'Completed Beginning of Turn'
		else
			conds = player[:conditions].dup
			conds.each do |cond|
				cond = cond.split(/[be]on?t /)[0]
				cond = cond.split(/ save ends/)[0]
				cond = cond.split(/ se/)[0]
				cond = cond.split(/ - /)[0]
				if cond =~ /^Ongoing (.*)$/i then
					do_player_edit player, $1.to_s
				end
			end
		end
		@@chars.each do |char|
			char[:conditions].delete_if {|cond| cond =~ /bot #{Regexp.escape(player[:name])}/i}
		end
		@@clog_logging = old_logging
	end

	def end_of_turn player
		old_logging = @@clog_logging
		@@clog_logging = false
		@@chars.each do |char|
			char[:conditions].delete_if {|cond| cond =~ /eot #{Regexp.escape(player[:name])}/i}
			char[:conditions].each {|cond| cond.sub! /eont #{Regexp.escape(player[:name])}/i, "eot #{player[:name]}"}
		end
		@@clog_logging = old_logging
	end
end
