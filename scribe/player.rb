class ::Scribe
	def players
		@@chars.each do |char|
			char[:slot] = player char
			char[:edit] = nil
		end
	end

	def refresh_players
		@@global_edit = nil
		@@player_stack.clear
		@@player_stack.append { players }
	end

	def player(p)
		font_size = 12
		font_margin = [7,5,2,5]
		this_slot = flow :margin=>3 do
			flow :width=>0.1 do
				flow :margin=>[
						font_margin[0],
						font_margin[1],
						15,
						font_margin[3]
					] do
					para p[:init].to_s,
						:margin=>0,
						:align=>"right",
						:font=>font_size.to_s,
						:stroke=>gray
				end
			end
			stack :width=>0.4 do
				curhp = p[:maxhp] - p[:hp]
				if (p[:selected]) then
					background darkseagreen, :curve=>6
					border coral, :curve=>6
				elsif (curhp > 0 and curhp < p[:maxhp]/2)
					background crimson, :curve=>6
					border coral, :curve=>6
				elsif (not p[:is_mob] and curhp <= 0 and curhp > -(p[:maxhp]/2))
					background dimgray, :curve=>6
					border coral, :curve=>6
				elsif (curhp <= 0 and (p[:is_mob] or curhp <= -(p[:maxhp]/2)))
					background dimgray, :curve=>6
					border crimson, :curve=>6, :strokewidth=>5
				else
					background darkgray, :curve=>6
					border coral, :curve=>6
				end
				flow :margin=>[7,5,2,0] do
					name_match, name_unmatch = longest_common_prefix(p[:name],@@player_select)
					flow :width=>0.9 do
						para name_match,
							:margin=>0,
							:align=>"left",
							:font=>font_size.to_s,
							:stroke=>darkred
						para name_unmatch,
							:margin=>0,
							:align=>"left",
							:font=>font_size.to_s,
							:stroke=>black
						click do |btn,x,y|
							if (btn == 1) then
								player_click(p[:name])
							elsif (btn == 3) then
								p[:selected] = p[:selected]?false : true
								refresh_players
								open_global_edit if @@chars.detect{|char| char[:selected]}
							end
						end
					end
					flow :width=>0.1 do
						del_link = para "[d]",
							:margin=>0,
							:align=>"right",
							:font=>font_size.to_s,
							:stroke=>black
						click do
							do_player_edit p, "delete" if confirm "Really delete #{p[:name]}?"
							refresh_players
						end
						hover { del_link.style(:stroke => red) }
						leave { del_link.style(:stroke => black) }
					end
				end
				stack :margin=>[14,0,2,5] do
					p[:conditions].each_with_index do |cond,cond_idx|
						flow do
							flow :width=>0.9 do
								para cond,
									:margin=>0,
									:font=>"italic "+(font_size*0.7).to_s,
									:stroke=>gray(0.2)
							end
							flow :width=>0.1 do 
								del_link = para "[d]", :margin=>0, :font=>(font_size*0.7).to_s, :stroke=>gray(0.2)
								click do
									do_player_edit p, "c-: ##{cond_idx.to_s}"
									refresh_players
								end
								hover { del_link.style(:stroke => red) }
								leave { del_link.style(:stroke => black) }
							end
						end
					end
				end unless p[:conditions].empty?
			end
			flow :width=>0.4 do
				p[:temphp] = 0 if p[:temphp].nil?
				p[:maxhp] = 0 if p[:maxhp].nil?
				p[:hp] = 0 if p[:hp].nil?
				hp_str = if (p[:is_mob] == true) then
					p[:hp].to_s
				else
					if (p[:temphp] > 0) then
						"#{(p[:maxhp]-p[:hp]).to_s} [#{p[:temphp].to_s}]/#{p[:maxhp]}"
					else
						"#{(p[:maxhp]-p[:hp]).to_s}/#{p[:maxhp]}"
					end
				end
				#border black
				flow do
				hp_bar p[:hp], p[:maxhp] unless p[:is_mob]
				para hp_str,
					:margin=>font_margin,
					:align=>"left",
					:font=>font_size.to_s
				end
			end
			flow :width=>0.1 do
				para p[:ibonus].to_s,
					:margin=>font_margin,
					:align=>"left",
					:font=>font_size.to_s
			end
			arrow(width*0.1-12,10,10,10) if p[:active]
		end
		return this_slot
	end
	def arrow(left,top,width,height)
		stroke saddlebrown
		strokewidth 2
		line left,top,left+width,top+height/2.0
		line left+width,top+height/2.0,left,top+height
		#rect top,left,top+height,left+width
	end
	def hp_bar(hp,max_hp)
		is_dead = (hp > max_hp)
		portion = hp*1.0/max_hp
		portion = 2*(portion-1) if is_dead
		debug [hp,max_hp].inspect
		debug is_dead
		portion = 0 if portion < 0
		portion = 1 if portion > 1
		bar_height = 30
		bottom_bar = (bar_height*(1.0-portion)).to_i
		top_bar = (bar_height*(portion)).to_i
		stack :margin=>3, :height=>bar_height+6, :width=>11 do
			stack :height=>top_bar, :width=>5 do
				background darkgray
			end
			stack :height=>bottom_bar, :width=>5 do
				background is_dead ? crimson : red
			end
		end
	end

	def longest_common_prefix(a,b)
		idx = 0
		prefix = ''
		while (idx < a.length and idx < b.length and a.downcase[idx] == b.downcase[idx]) do
			prefix << a[idx]
			idx += 1
		end
		a_cdr = a[idx..-1]
		b_cdr = b[idx..-1]
		#debug [prefix, a_cdr, b_cdr].inspect
		return prefix, a_cdr, b_cdr
	end

	def player_click(name)
		p = @@chars.detect{|char| char[:name] == name}
		return if p.nil?

		if (p[:edit].nil?) then
			p[:slot].append do
				p[:edit] = flow do 
					dmg_edit = edit_line "", :margin=>10, :width=>1.0
					keypress do |key|
						if (key == "\n" or key == :enter) then
							do_player_edit p, dmg_edit.text
							refresh_players
						end
					end
				end
			end
		else
			p[:edit].remove
			p[:edit] = nil
		end
	end
end
